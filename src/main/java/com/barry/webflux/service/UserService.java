package com.barry.webflux.service;

import com.barry.webflux.hangler.dto.BeneficiaryRequestDto;
import com.barry.webflux.hangler.dto.UserRequestDto;
import com.barry.webflux.hangler.exception.ApplicationException;
import com.barry.webflux.model.Beneficiary;
import com.barry.webflux.model.Gift;
import com.barry.webflux.model.User;
import com.barry.webflux.repository.GiftRepository;
import com.barry.webflux.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final GiftRepository giftRepository;

    public Flux<User> findAll(){
        return userRepository.findAll();
    }

    public Mono<User> findUserById(String  userId){
        return userRepository.findById(userId).log();
    }

    public Mono<User> save(UserRequestDto userDto){
        return userRepository.existsByUsername(userDto.getEmail())
                .flatMap(isExist->{
                    if(Boolean.TRUE.equals(isExist)){
                        return Mono.error(new ApplicationException("the user already exists"));
                    }
                    User user = new User()
                            .setEmail(userDto.getEmail())
                            .setUsername(userDto.getEmail())
                            .setAge(userDto.getAge())
                            .setCountry(userDto.getCountry());
                    return userRepository.save(user);
                }).log();
    }

    public Mono<User> update(UserRequestDto userRequestDto, String userId){
        return userRepository.findById(userId)
                .flatMap(userMono->{
                    userMono.setAge(userRequestDto.getAge());
                    userMono.setCountry(userRequestDto.getCountry());
                    return userRepository.save(userMono);
                })
                .switchIfEmpty(Mono.error(new RuntimeException("User not found")))
                .log();

    }

    public Mono<Void> deleteUserById(String userId) {
        return userRepository.deleteById(userId);
    }

    public Mono<Void> deleteAll() {
        return userRepository.deleteAll();
    }

    public Mono<User> addBeneficiaryToUser(BeneficiaryRequestDto beneficiaryDto, String userId){
        return userRepository.findById(userId)
                .flatMap(userMono->{
                Beneficiary beneficiary = Beneficiary.builder()

                            .id(UUID.randomUUID().toString())
                            .firstname(beneficiaryDto.getFirstname())
                            .lastName(beneficiaryDto.getLastName())
                            .relationship(beneficiaryDto.getRelationship())
                        .build();
                    userMono.addBeneficiary(beneficiary);
                    return userRepository.save(userMono);
                })
                .switchIfEmpty(Mono.empty());
    }

    public Mono<User> removeBeneficiary(String beneficiaryId, String userId){
        return userRepository.findById(userId)
                .flatMap(userMono->{
                userMono.removeBeneficiary(beneficiaryId);
                    return userRepository.save(userMono);
                })
                .switchIfEmpty(Mono.error(() -> new RuntimeException(" User not found")));
    }

    public Mono<Beneficiary> getBeneficiary(String beneficiaryId, String userId){
        return userRepository.findById(userId)
                .flatMap(userMono-> Mono.just(userMono.getBeneficiary(beneficiaryId)))
                .switchIfEmpty(Mono.error(() -> new RuntimeException(" User not found")));
    }

    public Mono<Gift> addGiftToUser(String userId, Gift  gift){
        Mono<User> userMono = userRepository.findById(userId);

       return userMono
                .flatMap(user -> Mono.just(gift)
                       .flatMap(gits->{
                           gits.setLocalDate(LocalDate.now());
                           gits.setUser(user);
                           return giftRepository.save(gits);
                       }));




    }

}
