package com.barry.webflux.hangler;

import com.barry.webflux.hangler.dto.BeneficiaryRequestDto;
import com.barry.webflux.hangler.dto.UserRequestDto;
import com.barry.webflux.hangler.validator.ValidatorHandler;
import com.barry.webflux.model.User;
import com.barry.webflux.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;
import static org.springframework.web.reactive.function.server.ServerResponse.*;

@Component
@RequiredArgsConstructor
@Slf4j
public class UserHandler {

    public static final String USER_ID = "userId";
    public static final String BENEFICIARY_ID = "beneficiaryId";
    private final UserService userService;
    private final ValidatorHandler validatorHandler;

    public Mono<ServerResponse> getUser(ServerRequest serverRequest){

        String uuid = serverRequest.pathVariable(USER_ID);

        Mono<User> userMono = userService.findUserById(uuid);

        return userMono.flatMap(user -> ok().contentType(MediaType.APPLICATION_JSON).bodyValue(user))
                    .switchIfEmpty(noContent().build(Mono.empty()));

    }

    public Mono<ServerResponse> getAllUsers(ServerRequest serverRequest){

        return userService.findAll().collectList().flatMap(users -> {
            if(users.isEmpty()){
                return noContent().build();
            }

            return ok().contentType(MediaType.APPLICATION_JSON)
                    .body(fromValue(users));
        });

    }

    public Mono<ServerResponse> saveUser(ServerRequest serverRequest){

        return serverRequest.bodyToMono(UserRequestDto.class)
                .doOnNext(validatorHandler::validate)
                .flatMap(userService::save)
                .doOnSuccess(userSaved -> log.info("Creating user with username" + userSaved.getUsername()))
                .doOnError(Mono::error)
                .flatMap(user-> created(getToUri(user)).bodyValue(user))
                .log();
    }

    public Mono<ServerResponse> updateUser(ServerRequest serverRequest){

        String uuid = serverRequest.pathVariable(USER_ID);

        return serverRequest.bodyToMono(UserRequestDto.class)
                .flatMap(user -> userService.update(user, uuid))
                .flatMap(user -> ok().contentType(MediaType.APPLICATION_JSON).bodyValue(user));

    }

    public Mono<ServerResponse> deleteUserById(ServerRequest  serverRequest){

        String uuid = serverRequest.pathVariable(USER_ID);
        return userService.deleteUserById(uuid)
              .then(noContent().build())
              .switchIfEmpty(noContent().build());
    }


    public Mono<ServerResponse> addBeneficiaryToUser(ServerRequest serverRequest){
        String userId = serverRequest.pathVariable(USER_ID);
        Mono<BeneficiaryRequestDto> beneficiaryMono = serverRequest.bodyToMono(BeneficiaryRequestDto.class);

        return beneficiaryMono
                .doOnNext(validatorHandler::validate)
                .flatMap(beneficiary -> userService.addBeneficiaryToUser(beneficiary, userId))
                .flatMap(user -> ok().contentType(MediaType.APPLICATION_JSON).bodyValue(user))
                .doOnSuccess(beneficiary-> log.info("its ok"))
                .doOnError(Mono::error);


    }

    public Mono<ServerResponse> removeBeneficiaryToUser(ServerRequest serverRequest){
        String userId = serverRequest.pathVariable(USER_ID);
        String beneficiaryId = serverRequest.pathVariable(BENEFICIARY_ID);

        return  userService.removeBeneficiary(beneficiaryId, userId)
                .flatMap(user -> ok().contentType(MediaType.APPLICATION_JSON).bodyValue(user))
                .doOnSuccess(user-> log.info("its ok"))
                .doOnError(Mono::error);


    }

    private static URI getToUri(User user) {
        return UriComponentsBuilder
                .fromPath("/{userId}")
                .buildAndExpand(user).toUri();
    }


}
