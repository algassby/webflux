package com.barry.webflux.hangler;

import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Component
public class UserRoutingHandler {

    private static final String USER_ENDPOINT = "/api/v1/users";
    private static final String USER_ID = "/{userId}";

    @Bean
    RouterFunction<ServerResponse> routerFunction(UserHandler userHandler){
        return route(GET(USER_ENDPOINT), userHandler::getAllUsers)
                .andRoute(GET(USER_ENDPOINT + USER_ID), userHandler::getUser)
                .andRoute(POST(USER_ENDPOINT + "/save").and(accept(MediaType.APPLICATION_JSON)), userHandler::saveUser)
                .andRoute(PUT(USER_ENDPOINT + USER_ID + "/update").and(accept(MediaType.APPLICATION_JSON)), userHandler::updateUser)
                .andRoute(DELETE(USER_ENDPOINT + USER_ID + "/delete"), userHandler::deleteUserById)
                .andRoute(POST(USER_ENDPOINT + USER_ID + "/addBeneficiary").and(accept(MediaType.APPLICATION_JSON)), userHandler::addBeneficiaryToUser)
                .andRoute(POST(USER_ENDPOINT + USER_ID + "/removeBeneficiary" + "/{beneficiaryId}"), userHandler::removeBeneficiaryToUser);
    }
}
