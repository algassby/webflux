package com.barry.webflux.hangler;

import com.barry.webflux.model.User;
import com.barry.webflux.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserService userService;

    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<User> streamUsers(){

        return userService
                .findAll()
                .flatMap(user -> Flux
                        .zip(Flux.interval(Duration.ofSeconds(2), Duration.ofSeconds(10)),
                                Flux.fromStream(Stream.generate(() ->user))
                        )
                        .log()
                        .map(Tuple2::getT2)

                )
                .log();
    }

    @GetMapping(value = "/stream-user", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<User> streamUser(@RequestParam(name = "userId", required = false) String userId){

        return userService
                .findUserById(userId)
                .switchIfEmpty(Mono.empty())
                .doOnError(Mono::error);




    }
}
