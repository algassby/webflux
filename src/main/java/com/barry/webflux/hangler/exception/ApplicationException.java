package com.barry.webflux.hangler.exception;

public class ApplicationException extends Exception{
    public ApplicationException(String message){
        super(message);
    }
}
