package com.barry.webflux.hangler.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.reactive.function.server.EntityResponse;
import org.springframework.web.server.ServerWebInputException;
import reactor.core.publisher.Mono;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@RestControllerAdvice
public class ControllerAdvice {


    @ExceptionHandler(ServerWebInputException.class)
    public Mono<EntityResponse<CustomErrorResponse>> HandleBadRequest(ServerWebInputException exception) {

        return EntityResponse.fromObject(CustomErrorResponse
                        .builder()
                        .traceId(exception.getDetailMessageCode())
                        .message(exception.getReason())
                        .timestamp(OffsetDateTime.now(ZoneOffset.of("+02:00")))
                        .status(HttpStatus.BAD_REQUEST)
                        .build())
                .status(HttpStatus.BAD_REQUEST).build();
    }


}