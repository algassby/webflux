package com.barry.webflux.hangler;

import com.barry.webflux.model.Gift;
import com.barry.webflux.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

@RestController
@RequestMapping("/api/v1/gift")
@RequiredArgsConstructor
public class GiftController {

    private final UserService userService;
    private final Sinks.Many<Gift> giftSinks;
    private final Flux<Gift> giftFlux;


    @PostMapping("/update/{userId}")
    public Mono<Gift> updateGame(@RequestBody Gift gift, @PathVariable String userId) {

        return userService.addGiftToUser(userId, gift).flatMap(gift1 -> {
            giftSinks.tryEmitNext(gift1);
            return Mono.just(gift1);});
    }

    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Gift> getGameUpdates() {
        return this.giftFlux;
    }
}
