package com.barry.webflux.hangler.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class UserRequestDto {

    @Email
    private String email;
    @Positive(message = "Ne doit pas être negatif")
    private long age;
    private String country;
}
