package com.barry.webflux.hangler.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class BeneficiaryRequestDto implements Serializable {

    @NotBlank(message = "firstname required")
    @Length(min = 5, max = 20)
    private String firstname;
    @NotBlank(message = "lastName required")
    private String lastName;
    @NotBlank(message = "relationship required")
    private String relationship;

}
