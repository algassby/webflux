package com.barry.webflux.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document("gits")
@Data
public class Gift {

    @Id
    private String gitId;
    private String type;
    private LocalDate localDate;
    private User user;

}
