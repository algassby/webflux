package com.barry.webflux.model;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Vector;

@Document("users")
@Data
@Accessors(chain = true)
public class User {
    @Id
    private String userId;
    private String  username;

    @NotBlank(message = "Username is required.")
    @Email(message = "Must be a valid email.")
    private String email;
    @Positive(message = "Ne doit pas être negatif")
    private long age;
    private String country;
    private List<Beneficiary> beneficiaries = new Vector<>();

    public void addBeneficiary(Beneficiary beneficiary) {
         beneficiaries.add(beneficiary);
    }
    public void removeBeneficiary(String uuid) {
        beneficiaries.removeIf(beneficiary -> beneficiary.getId().equals(uuid));
    }
    public Beneficiary getBeneficiary(String uuid) {
      return   beneficiaries.stream().filter(beneficiary -> beneficiary.getId().equals(uuid))
                .findAny().orElseThrow(() -> new RuntimeException("beneificiary with id doest not found"));
    }
}
