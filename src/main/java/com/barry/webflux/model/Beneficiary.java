package com.barry.webflux.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Beneficiary {

    private String id;
    private String firstname;
    private String lastName;
    private String relationship;
}
