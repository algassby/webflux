package com.barry.webflux.config;

import com.barry.webflux.model.Gift;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

@Configuration
public class SinkConfig {

    @Bean
    public Sinks.Many<Gift> sink(){
        return Sinks.many().replay().limit(1);
    }

    @Bean
    public Flux<Gift> giftFlux(Sinks.Many<Gift> giftMany){
        return giftMany.asFlux();
    }
}
