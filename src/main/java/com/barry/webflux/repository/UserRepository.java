package com.barry.webflux.repository;

import com.barry.webflux.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface UserRepository extends ReactiveMongoRepository<User, String> {
    Mono<Boolean> existsByUsername(String username);
    Mono<User> findByUsername(String username);
}
