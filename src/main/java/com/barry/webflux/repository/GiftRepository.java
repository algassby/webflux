package com.barry.webflux.repository;

import com.barry.webflux.model.Gift;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GiftRepository extends ReactiveMongoRepository<Gift, String> {

}
